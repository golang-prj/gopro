package model

import (
	postgres "application/database"
)
type File struct {
	Filename   string    `json:"filename"`
	File string`json:"file"`
	Email string `json:"email"`

}
const queryInsert = "INSERT INTO files (filename, file, email) VALUES ($1,$2,$3);"

func (s *File) Upload() error {
	_,err := postgres.Db.Exec(queryInsert, s.Filename, s.File, s.Email)
	return err
}
func (s *File) Read() error{
	const queryGetUser = "SELECT * from files"
	return postgres.Db.QueryRow(queryGetUser,).Scan(&s.Filename,&s.File,&s.Email)
}
func GetAllfiles(email string) ([]File, error) {
	// const queryGetAll = "SELECT * FROM files ;"
	table, err := postgres.Db.Query("SELECT * FROM files where email = $1", email)
	if err != nil {
		return nil, err
	}
	fil := []File{}
	for table.Next() {
		var s File
		dbErr := table.Scan(&s.Filename,&s.File,&s.Email)
		if dbErr != nil {
			return nil, dbErr
		}
		fil = append(fil, s)

	}
	table.Close()
	return fil, nil
}

// 
func (s *File) DeleteFile(email string,Filename string) error {
	const queryDeleteFile = "DELETE  FROM files WHERE filename = $1 and email=$2 RETURNING filename;"
	return postgres.Db.QueryRow(queryDeleteFile, Filename, email).Scan(&s.Filename)
}
// func (s *File) UpdateFile(email string,Filename string) error {
// 	const queryDeleteFile = "Update files set filename=$1 WHERE filename = $1 and Email=$2 RETURNING filename;"
// 	return postgres.Db.QueryRow(queryDeleteFile, s.Filename,Filename ,email).Scan(&s.Filename)
// }