package model

import (
	postgres "application/database"
	"fmt"
)

type Userdata struct{
	Username string `json:"username"`
	Password string `json:"password"`
	Email string `json:"email"`
	Uimg string `json:"uimg"`
}
const queryInsertUser = "INSERT INTO userdata (Username, Password, email, uimg) VALUES ($1,$2,$3,$4);"

func (s *Userdata) Create() error {
	_,err := postgres.Db.Exec(queryInsertUser, s.Username, s.Password, s.Email, s.Uimg)
	return err
}

// logIn
const queryGetAdmin = "SELECT * FROM userData WHERE Email=$1;"
func (adm *Userdata) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email,
adm.Password).Scan(&adm.Email, &adm.Password)
}


func (a *Userdata) Check(email string) error {
	const queryCheck = "Select * from userdata where Email = $1;"
	err := postgres.Db.QueryRow(queryCheck,email).Scan(&a.Username,&a.Password,&a.Email,&a.Uimg)
	return err
}

func (adm *Userdata) GetProfile(email string) error {
	const queryGetProfile = "Select * from userdata where email = $1;"
	err := postgres.Db.QueryRow(queryGetProfile, email).Scan(&adm.Username, &adm.Email, &adm.Password, &adm.Uimg)
	return err
}

func (adm *Userdata) UpdateUser(email string) error {
	const queryUdateUser = "UPDATE userdata set username = $1, password=$2, uimg=$3 WHERE email=$4 RETURNING email"
	err := postgres.Db.QueryRow(queryUdateUser, adm.Username, adm.Password, adm.Uimg, email).Scan(&adm.Email)
	fmt.Println(err, "here")
	return err
}

