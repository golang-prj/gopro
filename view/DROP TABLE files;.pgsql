DROP TABLE files;

CREATE TABLE files (
  filename VARCHAR(255) NOT NULL,
  file text not null,
  username varchar(255) not NULl,
  FOREIGN KEY (username) REFERENCES userdata(email)
      ON DELETE CASCADE 
      ON UPDATE CASCADE
);