const loginsec = document.querySelector(".login-section");
const loginlink = document.querySelector(".login-link");
const registerlink = document.querySelector(".register-link");
registerlink.addEventListener("click", () => {
  loginsec.classList.add("active");
});
loginlink.addEventListener("click", () => {
  loginsec.classList.remove("active");
});

function signUp(){
// data to be sent to the POST request
  var data = {
    username : document.getElementById("username").value,
    // lastname : document.getElementById("lname").value,
    email : document.getElementById("email").value,
    password : document.getElementById("pw1").value,
    // pw : document.getElementById("pw2").value,
    uimg:""
  }
  // if (data.password !== data.pw) {
  //   alert("PASSWORD doesn't match!")
  //   return
  // }
//   // Basic verification
//   if (username.trim() === '' || email.trim() === '' || password1.trim() === '' || password2.trim() === '') {
//     alert('Please fill in all fields');
//     return;
//   }

//   // Additional verification checks
//   if (!isValidEmail(email)) {
//     alert('Invalid email format');
//     return;
//   }

//   if (!isStrongPassword(password1)) {
//     alert('Password must be at least 8 characters long and contain a combination of letters, numbers, and symbols');
//     return;
//   }

//   if (password1 !== password2) {
//     alert('Passwords do not match');
//     return;
//   }

//   // Proceed with sign-up
//   alert('Sign up successful!');
//   // You can redirect to another page or perform other actions here
// }

// // Helper functions for additional verification
// function isValidEmail(email) {
//   // Use a regular expression to validate email format
//   var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//   return emailRegex.test(email);
// }

// function isStrongPassword(password) {
//   // Password must be at least 8 characters long and contain a combination of letters, numbers, and symbols
//   var passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
//   return passwordRegex.test(password);
  console.log(data.email)
fetch("/signup", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  }).then((response) => {
    if (response.status == 201) {
      loginsec.classList.remove("active"); // Redirect to login section
    }
  });
 }


  // console.log("jjjj")


function login() {
  var _data = {
    email: document.getElementById("email1").value,
    password: document.getElementById("password").value,
  };
  fetch("/login", {
    method: "POST",
    body: JSON.stringify(_data),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => {
      if (response.ok) {
        localStorage.setItem("email",_data.email)
        window.open("home.html", "_self");
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch((e) => {
      if (e == "Error: Unauthorized") {
        alert(e + ". Credentials does not match!");
        return;
      }
    });
}