// Open profile modal
function openProfileModal() {
    const profileModal = document.getElementById('profileModal');
    profileModal.style.display = 'flex';
}

// Close profile modal
function closeProfileModal() {
    const profileModal = document.getElementById('profileModal');
    profileModal.style.display = 'none';
}
// // Attach event listeners after the eQ                                                                                                                                                              q1lements are rendered in the DOM
// window.addEventListener('DOMContentLoaded', () => {
//     const profileIcon = document.querySelector('.profile-icon');
//     profileIcon.addEventListener('click', openProfileModal);

//     const closeProfileButton = document.querySelector('.profile-modal-close');
//     closeProfileButton.addEventListener('click', closeProfileModal);

//     const profileForm = document.querySelector('.profile-modal-content');
//     profileForm.addEventListener('submit', updateProfile);

//     // Fetch initial data
//     fetchFiles();
//     fetchUserProfile();
// });



//// upload ___________________________________________________---



let email = localStorage.getItem("email")
// alert(email)

window.onload = function() {
    fetch("/file/"+email)
    .then(response => response.text())
    .then(data => showData(data))  

     fetch("/user/"+email)
   .then(res => res.text())
   .then(data => showDatas(data)) 
}

    
function showDatas(data) {
    var nod = JSON.parse(data)
    // document.querySelector(".pro").src = nod.uimg
    document.querySelector(".oii").innerHTML = nod.username
    // alert("haha")

}
function showData(data) {
    let getData = JSON.parse(data)
    
    getData.forEach((fil) => {
        let table = document.querySelector("#fileTable")

        let trs = document.createElement("tr")
        let td1 = document.createElement("td")
        td1.innerHTML = fil.filename
        trs.appendChild(td1)
        let td2 = document.createElement("td")
        td2.style.display="flex"
        td2.style.justifyContent="Space-between"
        // // let p1 = document.createElement("button")
        // // p1.innerHTML = "Update"
        // // p1.addEventListener('click', function() {
        // //     updatefile(this)
        // // });
        // td2.appendChild(p1)
        let p2 = document.createElement("button")
        p2.innerHTML = "Delete"
        p2.addEventListener('click', function() {
            deletefile(this)
        });
        p2.style.backgroundColor="Red"
        td2.appendChild(p2)
        let p3 = document.createElement("button")
        p3.style.backgroundColor="Green"
        p3.innerHTML = "Download"
        td2.appendChild(p3)
       

        trs.appendChild(td2)
        table.appendChild(trs)
    })
}

// document.querySelector("#file-input").addEventListener("change", function(){
   
//     const dataurl = new FileReader();
//     dataurl.addEventListener("load", ()=>{
//         localStorage.setItem("coursePicture", dataurl.result)
//     })
//     dataurl.readAsDataURL(this.files[0])
// })

document.querySelector("#file-input").addEventListener("change", function() {
  const file = this.files[0];
  const fileReader = new FileReader();
  
  fileReader.addEventListener("load", () => {
    const fileData = fileReader.result;
    localStorage.setItem("courseFile", fileData);
  });

  if (file) {
    if (file.type === "application/pdf" || file.type === "application/msword" || file.type === "application/vnd.ms-powerpoint") {
      fileReader.readAsDataURL(file);
    } else {
      console.log("Invalid file format. Please select a PDF, DOC, or PPT file.");
    }
  }
});


function upload() {
    // alert(localStorage.getItem("coursePicture"))
    var fileData = {
          filename: document.querySelector("#name-input").value,
	       file: localStorage.getItem("coursePicture"),
        email: email
    }

    console.log(fileData)
    if (fileData.filename == ""){
        alert("file name cannot be empty")
        return
    }

    fetch("/file", {
        method: "POST",
        body: JSON.stringify(fileData),
        headers: {'Content-Type': 'application/json'},
    })
     .then(response => {
            if (response.status == 201) {
                console.log("file uploaded successfully");
                window.open("home.html", "_self");
                
                // closeProfileModal(); // Close the modal after successful update
            } else {
                console.error('Failed to update profile');
            }
        })
        .catch(error => console.error(error));

}
// delete
function deletefile(r) {
    let selectedRow = r.parentElement.parentElement
    filename = selectedRow.cells[0].innerHTML

    if (confirm(`Are you sure you want to delete  the file ${filename}`)){
        fetch("/file/"+email+"/"+filename,{
            method:"DELETE",
            headers:{"Content-type":"application/json; charset=UTF-8"}
        }).then(response =>{
            if (response.ok){
                var rowIndex = selectedRow.rowIndex
                if (rowIndex > 0){
                    selectedRow.parentElement.deleteRow(selectedRow.rowIndex)
                }
                selectedRow = null
            }else{
                throw new Error(response.statusText)
            }
        }).catch(e =>{
            alert(e)
        })
    }
}
// function updatefile(r) {
//     selectedRow = r.parentElement.parentElement
//     document.getElementById("name-input").value = selectedRow.cells[0].innerHTML;
//     data = getFormData()
//     button = document.getElementById("button-add")
//     filename = selectedRow.cells[0].innerHTML
//     button.innerHTML = "Update"
//     button.setAttribute("onclick", update(filename))
// }
// function getFormData() {
//     var data = {
//         filename: document.getElementById("name-input").value,
//     }
//     return data
// }
// function resetForm() {
//     document.getElementById("name-input").value = "";
// }
// function update(filename) {
//     var data = getFormData()
//     console.log(data)
//     fetch("/file/"+email+"/"+filename, {
//         method:"PUT",
//         body:JSON.stringify(data),
//         headers:{"Content-type":"application/json; charset=UTF-8"}
//     }).then(response => {
//         if (response.ok){
//             selectedRow.cells[0].innerHTML = data.filename;
//             selectedRow.cells[1].innerHTML = data.file;
//             button = document.getElementById("button-add");
//             button.innerHTML = "Upload";
//             button.setAttribute("onclick",upload());
//             selectedRow = null;
//             console.log("here")
//             document.getElementById("name-input").innerHTML = "";
//             resetForm()
//         }    
//     }).catch(e =>{
//         alert(e)
//     })
// }