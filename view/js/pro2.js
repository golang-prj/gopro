var email = localStorage.getItem("email")
// alert(email)
       
       document.querySelector("#file").addEventListener('change', function(){
            const choosedFile = this.files[0];
            if (choosedFile) {
                const reader = new FileReader();
                reader.addEventListener('load', function(){
                    // img.setAttribute('src', reader.result);
                    localStorage.setItem("img", reader.result)
                });
                reader.readAsDataURL(choosedFile);
            }
        });
function save() {
    alert(localStorage.getItem("img"))
    var data = {
        username: document.querySelector("#usernameInput").value,
	    password: document.querySelector("#passwordInput").value,
	    uimg: localStorage.getItem("img")
    }

     fetch("/user/"+ email, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {"content-type": "application/json; charset:utf-8"}
    })
    .then(response => {
        if (response.ok) {
            alert("Your profile got updated")
            window.location.href = "profile.html"
        } else {
            throw new Error(response.statusText);
        }
    }).catch(e => {
        alert(e);
    });
}