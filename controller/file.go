package controller

import (
	"application/model"
	httpResp "application/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func Uploadhandler(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w,r) {
		return 
	}
	fmt.Println("hihihi")
    var file model.File
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&file)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest,"invalid json data")
		fmt.Println("errr",err)
		// w.Write([]byte(err.Error()))
		return 
	}
	fmt.Println("gggggh kkk",file)
	dberr :=file.Upload()
	if dberr != nil {
		httpResp.ResponseWithError(w,http.StatusBadRequest, dberr.Error())
		fmt.Println("2",dberr)
		return
	}
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"message":"added successfully"})
	fmt.Println("success")
}
func GetFilesHandler(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w,r) {
		return 
	}

	email := mux.Vars(r)["email"]
	card, getErr := model.GetAllfiles(email)
	if getErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
		httpResp.ResponseWithJson(w, http.StatusOK, card)

}
// delete
func DeleteFilesHandler(w http.ResponseWriter, r *http.Request) {
	//cookie verification
	if !VerifyCookie(w,r) {
		return 
	}

	filename := mux.Vars(r)["filename"]
	email := mux.Vars(r)["email"]

	var file model.File
	deleteErr := file.DeleteFile(email,filename)
	if deleteErr != nil {
		switch deleteErr {
		case sql.ErrNoRows:
			httpResp.ResponseWithError(w, http.StatusNotFound, "filesent is not registerd to the database")
		default:
			httpResp.ResponseWithError(w, http.StatusInternalServerError, deleteErr.Error())
		}
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, "File deleted")
}
// func UpdateFilesHandler(w http.ResponseWriter, r *http.Request) {
// 	filename := mux.Vars(r)["filename"]
// 	email := mux.Vars(r)["email"]

// 	var file model.File
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&file)
// 	if err != nil {
// 		fmt.Print("error in decoding the request")
// 		httpResp.ResponseWithError(w,http.StatusBadRequest,err.Error())
// 		return 
// 	}
// 	updateErr := file.UpdateFile(email,filename)
// 	if updateErr != nil {
// 		switch updateErr {
// 		case sql.ErrNoRows:
// 			httpResp.ResponseWithError(w,http.StatusNotFound,"The particular file is not registered in the database ")
// 		default:
// 			fmt.Print("Error in updating the data")
// 			httpResp.ResponseWithError(w,http.StatusBadRequest,err.Error())
// 		}
// 		return 
// 	}
// 	httpResp.ResponseWithJson(w,http.StatusOK,"updated successfully")
// }