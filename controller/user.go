package controller

import (
	"application/model"
	httpResp "application/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func AddUser(w http.ResponseWriter, r *http.Request){
	var user model.Userdata
	fmt.Println("ya")
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		httpResp.ResponseWithError(w,http.StatusBadRequest,"invalid json body")
		fmt.Println(err)
		return 
	}
	fmt.Println(user.Username)
	defer r.Body.Close()

	saveErr := user.Create()
	if saveErr != nil {
		httpResp.ResponseWithError(w,http.StatusBadRequest,saveErr.Error())
		fmt.Println(saveErr)
		return 
	}
	fmt.Println(user)
	httpResp.ResponseWithJson(w,http.StatusCreated,map[string]string{"status":"admin added"})
}


func LoginHandler(w http.ResponseWriter, r *http.Request){
	var admin model.Userdata
	if err := json.NewDecoder(r.Body).Decode(&admin); err != nil{
		httpResp.ResponseWithError(w,http.StatusBadRequest,"invalid json body")
		return 
	}
	email := admin.Email
	fmt.Print(email,"llll")
	var admin2 model.Userdata
	loginErr := admin2.Check(email)

	if loginErr != nil{
		switch loginErr{
		case sql.ErrNoRows:
			httpResp.ResponseWithError(w,http.StatusUnauthorized,"invalid login")
			fmt.Println("first",loginErr)
			
		default:
			httpResp.ResponseWithError(w,http.StatusBadRequest,"error in database")
			fmt.Print("second")
		}
		return 
	}
	if admin.Password != admin2.Password{
		httpResp.ResponseWithError(w,http.StatusUnauthorized,"invalid login")
		fmt.Print("third")
		return 
	}
	//create a cookie
	cookie := http.Cookie{
		Name: "admin-cookie",
		// Value: email +admin.Password,
		Value:"#@Furpa77",
		Expires: time.Now().Add(30 * time.Minute),
		Secure: true,
	}
	//set cookie and send back to client
	http.SetCookie(w,&cookie)
	httpResp.ResponseWithJson(w,http.StatusOK,map[string]string{"message":"successful"})
}
func VerifyCookie(w http.ResponseWriter,r *http.Request) bool {
	cookie, err := r.Cookie("admin-cookie")
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			httpResp.ResponseWithError(w,http.StatusSeeOther,"cookie not set")
		default:
			httpResp.ResponseWithError(w,http.StatusInternalServerError,"internal server error")
		}
		return false
	}
	if cookie.Value != "#@Furpa77" {
		httpResp.ResponseWithError(w,http.StatusSeeOther,"invalid cookie")
		return false
	}
	return true
}

func ProfileHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("reaching here")
	email := mux.Vars(r)["email"]
	var user model.Userdata
	getErr := user.GetProfile(email)
	if getErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "error in getting from database")
		fmt.Println("couldn't get from database")
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, user)
	fmt.Println("result", user)
}

func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var user model.Userdata
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	dbErr := user.UpdateUser(email)
	if dbErr != nil {
		switch dbErr {
		case sql.ErrNoRows:
			httpResp.ResponseWithError(w, http.StatusNotFound, "User not found")
		default:
			httpResp.ResponseWithError(w, http.StatusInternalServerError, dbErr.Error())
		}
	} else {
		httpResp.ResponseWithJson(w, http.StatusOK, user)
	}
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w,&http.Cookie{
		Name : "admin-cookie",
		Expires: time.Now(),
	})
	fmt.Println("logout successful")
	httpResp.ResponseWithJson(w,http.StatusOK,map[string]string{"message":"logout successful"})
}

