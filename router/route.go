package router

import (
	"application/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes(){
	// creating new router 
	router := mux.NewRouter()
	// register handler function with mux router
	router.HandleFunc("/signup" , controller.AddUser).Methods("POST")
	router.HandleFunc("/login",controller.LoginHandler).Methods("POST")
	router.HandleFunc("/user/{email}", controller.UpdateUserHandler).Methods("PUT")

	router.HandleFunc("/user/{email}", controller.ProfileHandler).Methods("GET")

	router.HandleFunc("/file",controller.Uploadhandler).Methods("POST")
	router.HandleFunc("/file/{email}", controller.GetFilesHandler).Methods("GET")
	// delete
	router.HandleFunc("/file/{email}/{filename}", controller.DeleteFilesHandler).Methods("DELETE")
	// router.HandleFunc("/file/{email}/{filename}", controller.UpdateFilesHandler).Methods("PUT")

	router.HandleFunc("/logout",controller.LogoutHandler).Methods("GET")

	

// 
    fileHandler := http.FileServer(http.Dir("view"))
	router.PathPrefix("/").Handler(fileHandler)
	// start the http server
	err := http.ListenAndServe(":8081", router)
	if err != nil{
		return
	}
}

